"use strict";
const burgerBtn = document.querySelector(".header__burger-menu");
const burgerList = document.querySelector(".header-nav");
burgerBtn.addEventListener("mouseup", function () {
  burgerList.classList.toggle("header-nav-mobile");
  burgerBtn.classList.toggle("burger-menu-active");
});
